import React, { createContext, useState } from "react"
import axios from "axios"
import { useHistory } from "react-router-dom"

export const MahasiswaContext = createContext()

export const MahasiswaProvider = props => {

    let history = useHistory()
    const [name, setName] = useState("John")
    const [dataMahasiswa, setDataMahasiswa] = useState([])
    const [input, setInput] = useState({
        name: "",
        course: "",
        score: 0
    })
    let [currentIndex, setCurrentIndex] = useState(-1)
    let [fetchStatus, setFetchStatus] = useState(true)


    const handleText = (param) => {
        let nilai = param

        if (nilai >= 80) {
            return "A"
        } else if (nilai >= 70 && nilai < 80) {
            return "B"
        } else if (nilai >= 60 && nilai < 70) {
            return "C"
        } else if (nilai >= 50 && nilai < 60) {
            return "D"
        } else {
            return "E"
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        let { name, course, score } = input

        if (currentIndex === -1) {
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name, course, score })
                .then((res) => {
                    history.push('/tugas14')
                    setFetchStatus(true)
                })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, { name, course, score }).then((res) => {
                history.push("/tugas14")
                setFetchStatus(true)
            })
        }

        setInput({
            name: "",
            course: "",
            score: 0
        })
        setCurrentIndex(-1)
    }






    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name

        setInput({ ...input, [name]: value })
    }


    const handleEdit = (event) => {
        let idMahasiswa = parseInt(event.target.value)

        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
            .then((res) => {
                let data = res.data
                setInput({
                    id: data.id,
                    name: data.name,
                    course: data.course,
                    score: data.score
                })
                setCurrentIndex(data.id)
            })

    }

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value)
        // console.log(idData)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
            .then(() => {
                setFetchStatus(true)
            })
    }


    let handleFunctions = {
        handleText,
        handleSubmit,
        handleChange,
        handleSubmit,
        handleEdit,
        handleDelete
    }

    let state = {
        dataMahasiswa, setDataMahasiswa,
        input, setInput,
        currentIndex, setCurrentIndex,
        fetchStatus, setFetchStatus,
    }


    return (
        <>
            <MahasiswaContext.Provider value={{ handleFunctions, state }}>
                {props.children}
            </MahasiswaContext.Provider>
        </>
    )

}