import React, { useEffect, useState } from "react"
import "../tugas-11/tugas11.css"
import axios from "axios"

const Tugas12 = () => {

    const [dataMahasiswa, setDataMahasiswa] = useState([])
    const [input, setInput] = useState({
        name: "",
        course: "",
        score: 0
    })
    let [currentIndex, setCurrentIndex] = useState(-1)
    let [fetchStatus, setFetchStatus] = useState(true)

        useEffect(() => {

            const fetchData = async () => {
                let { data } = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
                // console.log(data)
                let result = data.map((res) => {

                    let {
                        course,
                        id,
                        name,
                        score
                    } = res

                    return {
                        course,
                        id,
                        name,
                        score
                    }
                })
                setDataMahasiswa([...result])
                console.log(result)
            }


            if(fetchStatus){
                fetchData()
                setFetchStatus(false)
            }


        }, [fetchStatus, setFetchStatus])





    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name

        setInput({ ...input, [name]: value })
    }



    const handleSubmit = (event) => {
        event.preventDefault()

        let {name, course, score} = input

        if(currentIndex === -1){
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, {name,course, score })
            .then((res) => {
                setFetchStatus(true)
            })
        }else{
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, {name, course, score}).then((res) => {
               setFetchStatus(true)
            })
        }

        setInput({
            name: "",
            course: "",
            score: 0 
        })
        setCurrentIndex(-1)
    }

    const handleEdit = (event) => {
        let idMahasiswa = parseInt(event.target.value)

        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
            .then((res) => {
                let data = res.data
                setInput({
                    id: data.id,
                    name: data.name,
                    course: data.course,
                    score: data.score
                })
                setCurrentIndex(data.id)
            })

    }

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value)
        // console.log(idData)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then(() => {
            setFetchStatus(true)
        })
    }

    const handleText = (param) => {
        let nilai = param

        if (nilai >= 80) {
            return "A"
        } else if (nilai >= 70 && nilai < 80) {
            return "B"
        } else if (nilai >= 60 && nilai < 70) {
            return "C"
        } else if (nilai >= 50 && nilai < 60) {
            return "D"
        } else {
            return "E"
        }
    }

    return (
        <>


            <h1 style={{ textAlign: "center" }}>Daftar Harga buah</h1>
            <table id="tablebuah">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Index Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataMahasiswa !== undefined && (
                        <>
                            {
                                dataMahasiswa.map((res, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{res.name}</td>
                                            <td>{res.course}</td>
                                            <td>{res.score} </td>
                                            <td>{handleText(res.score)}</td>
                                            <td>
                                                <button onClick={handleEdit} value={res.id}>edit</button>
                                                <button onClick={handleDelete} value={res.id}>delete</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </>
                    )}
                </tbody>
            </table>
            <br />
            <div className="container-form">
                <form onSubmit={handleSubmit} method="post">
                    <label>Nama</label>
                    <input required onChange={handleChange} value={input.name} type="text" name="name" placeholder="Your name.." />

                    <label>Mata Kuliah</label>
                    <input required onChange={handleChange} value={input.course} type="text" name="course" placeholder="" />

                    <label>Nilai</label>
                    <input min={0} required onChange={handleChange} value={input.score} type="number" name="score" placeholder="" />

                    <input type="submit" value="Submit" />
                </form>
            </div>
        </>
    )

}

export default Tugas12