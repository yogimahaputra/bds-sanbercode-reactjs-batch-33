import React, { useContext, useEffect, useState } from "react"
import "../tugas-11/tugas11.css"
import axios from "axios"
import { MahasiswaContext } from "../context/MahasiswaContext"

const MahasiswaForm = () => {

    const { handleFunctions, state } = useContext(MahasiswaContext)
    let { handleText,
        handleChange,
        handleSubmit,
        handleEdit,
        handleDelete } = handleFunctions

    let {
        dataMahasiswa, setDataMahasiswa,
        input, setInput,
        currentIndex, setCurrentIndex,
        fetchStatus, setFetchStatus,
    } = state


        useEffect(() => {

            const fetchData = async () => {
                let { data } = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
                // console.log(data)
                let result = data.map((res) => {

                    let {
                        course,
                        id,
                        name,
                        score
                    } = res

                    return {
                        course,
                        id,
                        name,
                        score
                    }
                })
                setDataMahasiswa([...result])
                console.log(result)
            }


            if(fetchStatus){
                fetchData()
                setFetchStatus(false)
            }


        }, [fetchStatus, setFetchStatus])


    return (
        <>
            <br />
            <div className="container-form">
                <form onSubmit={handleSubmit} method="post">
                    <label>Nama</label>
                    <input required onChange={handleChange} value={input.name} type="text" name="name" placeholder="Your name.." />

                    <label>Mata Kuliah</label>
                    <input required onChange={handleChange} value={input.course} type="text" name="course" placeholder="" />

                    <label>Nilai</label>
                    <input min={0} required onChange={handleChange} value={input.score} type="number" name="score" placeholder="" />

                    <input type="submit" value="Submit" />
                </form>
            </div>
        </>
    )

}

export default MahasiswaForm