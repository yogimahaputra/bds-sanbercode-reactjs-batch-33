import React, { useContext, useEffect, useState } from "react"
import "../tugas-11/tugas11.css"
import axios from "axios"
import { MahasiswaContext } from "../context/MahasiswaContext"

const MahasiswaList = () => {

    const { handleFunctions, state } = useContext(MahasiswaContext)
    let { handleText,
        handleChange,
        handleSubmit,
        handleEdit,
        handleDelete } = handleFunctions

    let {
        dataMahasiswa, setDataMahasiswa,
        input, setInput,
        currentIndex, setCurrentIndex,
        fetchStatus, setFetchStatus,
    } = state

    useEffect(() => {

        const fetchData = async () => {
            let { data } = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            // console.log(data)
            let result = data.map((res) => {

                let {
                    course,
                    id,
                    name,
                    score
                } = res

                return {
                    course,
                    id,
                    name,
                    score
                }
            })
            setDataMahasiswa([...result])
            console.log(result)
        }


        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }


    }, [fetchStatus, setFetchStatus])


    return (
        <>


            <h1 style={{ textAlign: "center" }}>Daftar Harga buah</h1>
            <table id="tablebuah">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Index Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataMahasiswa !== undefined && (
                        <>
                            {
                                dataMahasiswa.map((res, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{res.name}</td>
                                            <td>{res.course}</td>
                                            <td>{res.score} </td>
                                            <td>{handleText(res.score)}</td>
                                            <td>
                                                <button onClick={handleEdit} value={res.id}>edit</button>
                                                <button onClick={handleDelete} value={res.id}>delete</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </>
                    )}
                </tbody>
            </table>
            <br />

        </>
    )

}

export default MahasiswaList