import React, { useContext } from "react"
import { Link } from "react-router-dom"
import "./nav.css"
import { ThemeContext } from "../context/themeContext"

const Nav = () => {

    const { theme, setTheme } = useContext(ThemeContext)
    console.log(theme)

    return(
        <>
            <nav className={theme} >
                <li>
                    <Link to="/">Tugas9</Link>
                </li>
                <li>
                    <Link to="/tugas10">Tugas10</Link>
                </li>
                <li>
                    <Link to="/tugas11">Tugas11</Link>
                </li>
                <li>
                    <Link to="/tugas12">Tugas12</Link>
                </li>
                <li>
                    <Link to="/tugas13">Tugas13</Link>
                </li>
                <li>
                    <Link to="/tugas14">Tugas14</Link>
                </li>
            </nav>
        </>
    )
}

export default Nav