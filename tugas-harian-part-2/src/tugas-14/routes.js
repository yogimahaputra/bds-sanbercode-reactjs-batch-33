import React from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"
import { MahasiswaProvider } from "../context/MahasiswaContext"
import { ThemeProvider } from "../context/themeContext"
import Tugas10 from "../tugas-10/tugas10"
import Tugas11 from "../tugas-11/Tugas11"
import Tugas12 from "../tugas-12/tugas12"
import Mahasiswa from "../tugas-13/mahasiwa"
import Nav from "./nav"
import Tugas14Form from "./tugas14Form"
import Tugas14List from "./tugas14List"

const Router = () => {


    return (
        <>
            <BrowserRouter>
                <MahasiswaProvider>
                    <ThemeProvider>

                        <Nav />
                        <Switch>

                            <Route exact path={'/'} component={Tugas10} />
                            <Route exact path={'/tugas11'} component={Tugas11} />
                            <Route exact path={'/tugas12'} component={Tugas12} />
                            <Route exact path={'/tugas13'} component={Mahasiswa} />
                            <Route exact path={'/tugas14'} component={Tugas14List} />
                            <Route exact path={'/tugas14/create'} component={Tugas14Form} />
                            <Route exact path={'/tugas14/edit/:slug'} component={Tugas14Form} />

                        </Switch>

                    </ThemeProvider>

                </MahasiswaProvider>
            </BrowserRouter>

        </>
    )

}

export default Router