import React, { useContext, useEffect, useState } from "react"
import "../tugas-11/tugas11.css"
import axios from "axios"
import { MahasiswaContext } from "../context/MahasiswaContext"
import { useHistory } from "react-router-dom"
import { useParams } from "react-router-dom"

const Tugas14Form = () => {
    let history = useHistory()
    const { handleFunctions, state } = useContext(MahasiswaContext)
    // let { handleSubmit } = handleFunctions

    let {
        dataMahasiswa, setDataMahasiswa,
        input, setInput,
        currentIndex, setCurrentIndex,
        fetchStatus, setFetchStatus,
    } = state

    const { slug } = useParams()
    console.log(slug)

    useEffect(() => {

        if (slug !== undefined) {
            axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${slug}`)
                .then((res) => {
                    let data = res.data
                    setInput({
                        id: data.id,
                        name: data.name,
                        course: data.course,
                        score: data.score
                    })
                    setCurrentIndex(data.id)
                })
        }


        return () => {
            setInput({
                name: "",
                course: "",
                score: 0
            })
            setCurrentIndex(-1)
        }

    }, [])



    const handleSubmit = (event) => {
        event.preventDefault()

        let { name, course, score } = input

        if (currentIndex === -1) {
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name, course, score })
                .then((res) => {
                    history.push('/tugas14')
                    setFetchStatus(true)
                })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, { name, course, score }).then((res) => {
                history.push("/tugas14")
                setFetchStatus(true)
            })
        }

        setInput({
            name: "",
            course: "",
            score: 0
        })
        setCurrentIndex(-1)
    }


    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name

        setInput({ ...input, [name]: value })
    }




    return (
        <>


            <br />
            <div className="container-form">
                <form onSubmit={handleSubmit} method="post">
                    <label>Nama</label>
                    <input required onChange={handleChange} value={input.name} type="text" name="name" placeholder="Your name.." />

                    <label>Mata Kuliah</label>
                    <input required onChange={handleChange} value={input.course} type="text" name="course" placeholder="" />

                    <label>Nilai</label>
                    <input min={0} required onChange={handleChange} value={input.score} type="number" name="score" placeholder="" />

                    <input type="submit" value="Submit" />
                </form>
            </div>
        </>
    )

}

export default Tugas14Form