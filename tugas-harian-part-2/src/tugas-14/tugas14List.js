import React, { useContext, useEffect, useState } from "react"
import "../tugas-11/tugas11.css"
import axios from "axios"
import { MahasiswaContext } from "../context/MahasiswaContext"
import { Link } from "react-router-dom"
import { useHistory } from "react-router-dom"
import SwitchTheme from "./swtichTheme"

const Tugas14List = () => {

    let history = useHistory()
    const { handleFunctions } = useContext(MahasiswaContext)
    let {handleText} = handleFunctions
    const [dataMahasiswa, setDataMahasiswa] = useState([])
    const [input, setInput] = useState({
        name: "",
        course: "",
        score: 0
    })
    let [currentIndex, setCurrentIndex] = useState(-1)
    let [fetchStatus, setFetchStatus] = useState(true)

        useEffect(() => {

            const fetchData = async () => {
                let { data } = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
                // console.log(data)
                let result = data.map((res) => {

                    let {
                        course,
                        id,
                        name,
                        score
                    } = res

                    return {
                        course,
                        id,
                        name,
                        score
                    }
                })
                setDataMahasiswa([...result])
                console.log(result)
            }


            if(fetchStatus){
                fetchData()
                setFetchStatus(false)
            }


        }, [fetchStatus, setFetchStatus])



    const handleEdit = (event) => {
        let idMahasiswa = parseInt(event.target.value)

        history.push(`/tugas14/edit/${idMahasiswa}`)

    }

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value)
        // console.log(idData)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then(() => {
            setFetchStatus(true)
        })
    }



    return (
        <>
            <SwitchTheme/>
            <h1 style={{ textAlign: "center" }}>Daftar Harga buah</h1>
            <Link to="/tugas14/create"><button>Create data</button></Link>
            <table id="tablebuah">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Index Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataMahasiswa !== undefined && (
                        <>
                            {
                                dataMahasiswa.map((res, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{res.name}</td>
                                            <td>{res.course}</td>
                                            <td>{res.score} </td>
                                            <td>{handleText(res.score)}</td>
                                            <td>
                                                <button onClick={handleEdit} value={res.id}>edit</button>
                                                <button onClick={handleDelete} value={res.id}>delete</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </>
                    )}
                </tbody>
            </table>
            <br />

        </>
    )

}

export default Tugas14List